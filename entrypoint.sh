#!/bin/bash

set -e

CONF_TEMPLATE=/etc/odoo.conf.tmpl
ODOO_RC=/tmp/odoo.conf

mandatory_vars=("DB_USER" "DB_PORT" "DB_PASSWORD" "DB_HOST")
function check_mandatory_vars() {
    for varname in ${mandatory_vars[@]}; do
        if [[ -z "${!varname}" ]]; then
            echo "Mandatory env var ${varname} is empty"
            echo "The following variables must be set: ${mandatory_vars[@]}"
            exit 1
        fi
    done
}

function replace_conf() {
    envsubst < ${CONF_TEMPLATE} > ${ODOO_RC}
    export ODOO_RC
}

check_mandatory_vars
DB_ARGS="--db_host ${DB_HOST} --db_port ${DB_PORT} --db_user ${DB_USER} --db_password ${DB_PASSWORD}"
replace_conf

case "$1" in
    -- | odoo)
        shift
        if [[ "$1" == "scaffold" ]] ; then
            exec odoo "$@"
        else
            wait-for-psql.py ${DB_ARGS} --timeout=30
            exec odoo "$@"
        fi
        ;;
    -*)
        wait-for-psql.py ${DB_ARGS} --timeout=30
        exec odoo "$@"
        ;;
    *)
        exec "$@"
esac

exit 1
